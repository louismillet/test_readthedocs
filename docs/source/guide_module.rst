How to create a module
======================

Import the base and module directory
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

First you have to go to this directories : 

https://bitbucket.org/uqokorn/s4m_base/src/master/
https://bitbucket.org/uqokorn/s4m_modules/src/master/

(Don't forget to add your SSH public key to your Bitbucket account.)

Take the SSH clone (not the HTTPS) (buttom at top-right name "clone").

After you clone it into your Shell / Git Bash / .... under the path you want.

e.g : :: 
    
    ~/scratch/s4m



Then you will see a "s4m_modules" and a "s4m_base" directories.
Go to the "s4m_modules" and create symbolics links for "_core" and "_selftest".

e.g : ::
    
    $ ln -s ../s4m_base/modules/_core
    $ ln -s ../s4m_base/modules/_selftest


Now you have to create a config file or modify the old one to make sure that the s4m commands will run your modules on the pipe and not the one already install (much better to test your code). ::

    $ cd ~/.s4m
    $ vim config_dev

And put the path to your s4m "base" and "modules" directories

e.g : ::

    S4M_SCRIPT_HOME=/home/<User_name>/scratch/s4m/s4m_base
    S4M_MODULE_HOME=/home/<User_name>/scratch/s4m/s4m_modules




Create your modules
^^^^^^^^^^^^^^^^^^^

To start create a module you have to create your own branch using git command : (Of course under your s4m_module location) ::
    
    $ git checkout -b <branch_name>

And after you can create your own modules. To make it simpler you can use the command : ::
    
    $ s4m -C <Way_to_your_config_file> addmodule -n <Module_name>

It will create all the files you will need and some advices to help you for the development of your module.
    - module.ini
    - <Module_name>.sh  
    - install.sh


After this it is free to you to create all programs you want (<Module_name>.sh is the one which get some advices for developing modules).
It is highly recommand to do a "test_<Module_name>.sh" to test easily your code.
To add flags to your command you have to edit the module.ini all is describ in it.


Put your job on Bitbucket
^^^^^^^^^^^^^^^^^^^^^^^^^

To put your job on bitbucket you have to : ::
    
    $ git add <Files_Name_you_want_to_add>
    $ git commit -m "a message which explain what your commit do"

And if it is the first time that you push you have to : ::
    
    $ git push origin <branch_name>

And if you have already push once you can just write the command : ::
    
    $ git push

TIPS : 

You can follow the progress of your add / commit / push with : ::
    
    $ git status

